from schema import Schema, And, Or


LOG_PATH = '/tmp/tingro.log'
MEDIA_PATH = '/tmp/tingro_media'

devices = {
    "light":
        {
            "controllable": True,
            "switch_type": "OnOff",
            "name": "light",
            "pin": 16,
            "port": 8001,
            "messages":
                [
                    Schema(And(str, lambda x: x in ['on', 'off']))
                ]

        },
    "pump":
        {
            "controllable": True,
            "switch_type": "OnPulse",
            "name": "pump",
            "pin": 20,
            "port": 8002,
            "messages": [
                Schema(And(int, lambda x: 1 <= x <= 10))
            ]
        }
}

TOPIC_PATTERN_CONTROL = "dev/{}/control"
TOPIC_PATTERN_STATUS = "dev/{}/status"
MQTT_HOST = "raspberrypi"
MQTT_PORT = 1883
MQTT_TIMEOUT = 60

PUMPING_MAX_DURATION = 10
