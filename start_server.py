import logging
from logging import handlers
import os
from config import LOG_PATH
from rpc import Light, Pump
from nameko.runners import ServiceRunner

handler = logging.handlers.WatchedFileHandler(
    os.environ.get("LOGFILE", LOG_PATH))

formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
handler.setFormatter(formatter)
root = logging.getLogger()
root.setLevel(os.environ.get("LOGLEVEL", "INFO"))
root.addHandler(handler)
logging.info("Starting server...")

runner = ServiceRunner(config={"AMQP_URI": "amqp://pi:pi@raspberrypi", "remote_addr": "raspberrypi"})

runner.add_service(Light)
runner.add_service(Pump)
runner.start()

