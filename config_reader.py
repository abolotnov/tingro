import yaml
import os
config_file = 'rpc/config.yaml'


class Reader:

    def __init__(self):
        basedir = os.path.dirname(os.path.abspath(__file__))
        print(basedir)
        with open(os.path.join(basedir, config_file), "r") as file:
            self.config = yaml.load(file, Loader=yaml.FullLoader)

    def get_property(self, prop, as_dict=False):
        try:
            if as_dict:
                return {prop: self.config[prop]}
            else:
                return self.config[prop]

        except Exception:
            raise KeyError("Configuration key {} does not exist".format(prop))

