#!/usr/bin/env bash
cp /home/pi/tingro/rpi-files/tingro-rpc.service /lib/systemd/system
cp /home/pi/tingro/rpi-files/tingro-web.service /lib/systemd/system

chmod u+x /home/pi/tingro/start-all.sh

systemctl enable tingro
systemctl start tingro


