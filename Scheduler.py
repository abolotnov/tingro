from apscheduler.schedulers.blocking import BlockingScheduler
import json
from datetime import datetime
from nameko.standalone.rpc import ClusterRpcProxy
import logging
from config_reader import Reader

logging.basicConfig()
logging.getLogger('apscheduler').setLevel(logging.DEBUG)


rpc_config = Reader().get_property('AMQP_URI', True)


def check_pump():
    print("starting pump check")
    with open("schedule.json", "r") as file:
        data = json.load(file)["pump"]
        pump_duration_this_hour = int(data[str(datetime.now().hour)])
        with ClusterRpcProxy(rpc_config) as rpc:
            if pump_duration_this_hour > 0:
                rpc.pump.on(pump_duration_this_hour)
                rpc.event.post("scheduler", "watering on schedule", "ok", "schedule.json")
            else:
                rpc.event.post("scheduler", "no water expected at this hour, not starting pump", "ok", "schedule.json")


def check_light():
    print("Starting light check")
    with open("schedule.json", "r") as file:
        data = json.load(file)["light"]
        light_this_hour = data[str(datetime.now().hour)]
        with ClusterRpcProxy(rpc_config) as rpc:
            if light_this_hour == "on":
                rpc.light.on()
                rpc.event.post("scheduler", "light on schedule is on - turned on", "ok", "schedule.json")
            else:
                rpc.light.off()
                rpc.event.post("scheduler", "light on schedule is off - turned off", "ok", "schedule.json")


def report():
    print("{}, scheduler is alive".format(datetime.now()))


runner = BlockingScheduler()
runner.add_job(func=check_pump, trigger='interval', hours=1)
runner.add_job(func=check_light, trigger='interval', hours=1)
runner.add_job(func=report, trigger='interval', minutes=10)
print("Starting runner")
runner.print_jobs()
runner.start()
