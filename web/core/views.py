from django.shortcuts import render
from django.http import HttpResponse
from nameko.standalone.rpc import ClusterRpcProxy
import logging
from django.views.decorators.http import require_http_methods, require_POST
import logging
from web.settings import AMQP_config


def index(request):
    return render(request, 'index.html')


@require_POST
def event_add(request):
    return HttpResponse(status=200, content=request)


def events(request, device, count=1):
    return


def command(request, device, com, arg=None):
    logging.info("Got request: {}".format(request))
    try:
        with ClusterRpcProxy(AMQP_config) as rpc:
            """
            There is no implementation for capturing the message formats from services config and validating them at
            the api level, simple and dirty at this stage
            """
            if arg:
                response = getattr(getattr(rpc, device), com)(arg)
            else:
                response = getattr(getattr(rpc, device), com)()
        return HttpResponse(status=200, content=response)
    except Exception as e:
        logging.exception("Error executing command {} for {}".format(com, device))
        return HttpResponse(status=500, content=e)
