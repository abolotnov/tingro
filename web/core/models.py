from django.db import models


class Event(models.Model):
    device = models.CharField(max_length=32, null=False)
    command = models.CharField(max_length=32, null=False)
    date = models.DateTimeField(null=False)
    result = models.CharField(max_length=32, null=False)
    source = models.CharField(max_length=16, null=False, default="System")

    def __str__(self):
        return "{} to {}".format(self.command, self.device)
