from django.db.backends.signals import connection_created
"""
This hack keeps the database from locking.
Django is going to read-only, all the writing happens in rpc services
"""
def activate_foreign_keys(sender, connection, **kwargs):
    """Enable integrity constraint with sqlite."""
    if connection.vendor == 'sqlite':
        cursor = connection.cursor()
        cursor.execute('PRAGMA journal_mode=wal;')


connection_created.connect(activate_foreign_keys)