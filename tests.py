from nameko.standalone.rpc import ClusterRpcProxy
import time
from config_reader import Reader

cfg = Reader().get_property('AMQP_URI', True)

with ClusterRpcProxy(cfg) as rpc:

    #out = rpc.light.on()
    #print(out)
    #time.sleep(1)

    #out = rpc.light.off()
    #print(out)
    #time.sleep(1)

    #out = rpc.pump.on(1)
    #print(out)

    out = rpc.event.post("Test", "test", "test", "test")
    print(out)

print("End of tests")
