# Tingro automated plant grower system

RPI Zero W powered self-sustainable tiny grow station (software and hardware).
The project is aimed at building an affordable "grow box" with the following objectives:

- small portable "single box" setup, but flexible to scale to larger growing setups
- automatic lights management
- schedule-based watering
- automatic watering based on soil moisture measurements
- empty water tank signalling
- video on demand and time lapse to see how the plant is progressing
- video-based plant health monitoring (will have to involve AI)

## Current state

It works and I started testing it with a real plant. It does not have automatic installer/configurer so you'd have to tingle a little to set it up. I am planning to make it easier to setup in the future.

Hardware setup is pretty simple and I am working on describing it in more details and pictures, it's very affordable though and uses cheap components. I think my entire setup, including RPI Zero is around $20 a piece. 

## Hardware setup

- RPI Zero W
- 5V water pump
- MOSFET-powered trigger switches for light and pump
- 12V LED strip
- 12V power supply
- 12V to 5V converter

(pictures of the prototype setup will follow)

### My setup:

- I use 12v -> 5v convector to power everything (12V led strip and 5v everything else), you don't have to use it if you use 5V LED
- I use MOSFET triggers to control the led and the pump
- connect the LED trigger to pin 16 of RPI
- connect the PUMP trigger to pin 20 of RPI
- You can change the pin layout (they are hardcoded in ./rpc/Light and ./rpc/Pump - I will move these to config eventually)
- You can control the devices you use by importing (or removing imports) from `./rpc/__init__.py` - everything imported there will be picked up by the software and these things will be available for you to control
- I have a camera wired to RPI cam bus but no code for it to function yet - feel free to contribute

## Software setup

- Nameko rpc is used to send tasks to services
- Django hosts website for control and status details
- Nameko uses rabbitmq to communicate to services, this needs to be installed on RPI

## API

Local web server provides API for controlling devices

- /rpc/light/on or /rpc/light/off
- /rpc/pump/on/{duration} to control the watering pump

You can also control devices directly (see code in `Scheduler.py` for samples) via rpc proxy

## How to run this

At this point, I don't have automatic configuration for the OS image and dependencies (it's coming eventually), so the following would need to be done:

- install linux on your RPI
- configure your RPI to access local network via WIFI or connect it via ethernet
- login to your rpi via `ssh`
- clone this repo and `cd` to it
- install `rabbitmq`
- install dependencies: `pip install -r requirements.txt`
- make sure you add `PIGPIO_ADDR=<YOUR RPI HOST>` to the environment vars - this way you can run locally on remote GPIO (settings in repo are default for rasbian and work fine for both local execution and remote debugging)
- I committed django database into the repo, but if you want to use a different database, you'd need to `python manage.py migrate` (or `makemigrations` first)

Once you have done the above, you can run the system:

- `./run_rpc` - this will rur all the things that the system needs to function
- `./stop_all` will stop everything that you run in the previous command
- you can now navigate to the website for the system at <RPI address>:8000

current setup have scheduler running that uses the `schedule.json` and waters and controls light based on the schedule - if you change it, you need to re-run the scheduler (or stop everything and run_rpc again) 

You can also debug things remotely:

- use `./run_rpc.sh` to start nameko services (make sure the remote host is right - mine is `raspberrypi`)
- run web server as per above

^ Both of these can run locally on your box and when you send commands to devices, they will execute on your RPI.

## Next steps

- move all changeable things to config
- make config reader work with django
- update main page to show event log
- snap pictures and show the recent one on the main page
- build schedule configuration page to control watering and lights
- connect soil moisture sensor and use its input to water
- connect water sensor and alert on low water in the tank
- build some sort of push notifications
- move web to port 80
- build some basic security
- make the web app mobile-friendly with push notifications in it, too (AWP?)

## Notes and thoughts

- maybe just use ffserver to stream media? https://www.moreno.marzolla.name/software/linux-webcam-server/
- https://pypi.org/project/schema/ is very cool
