import gpiozero as gz
import logging
from time import sleep
from nameko.rpc import rpc
from .Codes import ActionResponse
from .Base import Base
import sqlite3
from datetime import datetime


class Event(Base):
    name = "event"

    def __init__(self):
        self.connection = sqlite3.connect(database='web/db.sqlite3')

    @rpc
    def post(self, device, command, result, source='unknown'):
        logging.info("got event to save: {}, {}, {}, {}".format(device, command, result, source))
        try:
            cursor = self.connection.cursor()
            sql = "INSERT INTO 'core_event' ('device', 'command', 'date', 'result', 'source') values ('{}', '{}', '{}', '{}', '{}');".format(
                device,
                command,
                datetime.now(),
                result,
                source
            )
            cursor.execute(sql)
            self.connection.commit()
            return ActionResponse.ok("event saved")

        except Exception as e:
            logging.critical(e)
            return ActionResponse.failed_to_execute("Error, see logs!")
