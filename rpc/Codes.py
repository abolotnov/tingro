import json


class ActionResponse:

    @staticmethod
    def ok(msg="Done"):
        return json.dumps({"OK": msg})

    @staticmethod
    def failed_to_execute(msg="Failed to execute command"):
        return json.dumps({"Device Failure": msg})

    @staticmethod
    def invalid_command(msg="Invalid command for this devices"):
        return json.dumps({"Invalid Command": msg})

    @staticmethod
    def custom(code: str, msg: str):
        return json.dumps({code: msg})
