import gpiozero as gz
import logging
from time import sleep
from nameko.rpc import rpc
from .Codes import ActionResponse
from .Base import Base
from config import PUMPING_MAX_DURATION


class Pump(Base):
    name = "pump"
    pin = 20
    device = gz.OutputDevice(pin)

    @rpc
    def on(self, duration):
        if not duration:
            logging.error("{} command need duration".format(self.name))
            return ActionResponse.invalid_command("Command need duration")
        try:
            int_duration = int(duration)
            if int_duration < 1 or int_duration > PUMPING_MAX_DURATION:
                logging.error("Pumping duration is either < 1 or > PUMPING_MAX_DURATION config value")
                return ActionResponse.invalid_command("pumping duration over max pumping setting")
            logging.info("{} got a command to pump for {} seconds, starting".format(self.name, int_duration))
            self.device.on()
            sleep(int_duration)
            self.device.off()
            logging.info("{} finished pumping".format(self.name))
            return ActionResponse.ok("done pumping")
        except Exception as e:
            logging.exception(e)
            return ActionResponse.failed_to_execute("something is not right: {}".format(e))
