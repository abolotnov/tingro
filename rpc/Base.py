from nameko.rpc import rpc


class Base:
    name = "Unknown"

    @rpc
    def status(self):
        return {"status": "Device did not implement status response"}

    @rpc
    def echo(self, value):
        return str(value)[::-1]
