import gpiozero as gz
import logging
from time import sleep
from nameko.rpc import rpc
from .Codes import ActionResponse
from .Base import Base
from config import PUMPING_MAX_DURATION
from .PiCameraAdapter import Adapter


class Camera(Base):
    name = "camera"
    pin = None
    device = Adapter()

    @rpc
    def snap(self):
        pass

