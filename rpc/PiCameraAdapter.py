import os
if os.uname().nodename == 'raspberrypi':
    from picamera import PiCamera as cam


class Adapter:
    _camera = None
    if cam:
        _camera = cam()

    def resolution(self, value):
        self._camera.resolution = value if self._camera else None




