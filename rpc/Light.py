import gpiozero as gz
import logging
from time import sleep
from nameko.rpc import rpc
from nameko.standalone.rpc import ClusterRpcProxy
from .Codes import ActionResponse
from .Base import Base
from config_reader import Reader
rpc_config = Reader().get_property('AMQP_URI', True)


class Light(Base):
    name = "light"
    pin = 16
    device = gz.OutputDevice(pin)

    def get_status(self):
        logging.info("Status requested, current device value is {}".format(self.device.value))
        if self.device.value == 1:
            return "on"
        elif self.device.value == 0:
            return "off"
        else:
            return "unknown"

    @rpc
    def start(self):
        logging.info("Starting {}: (turn on/off based on configured schedule, will blink for now)".format(self.name))
        for n in range(3):
            self.on()
            sleep(1)
            self.off()
            sleep(1)
        return ActionResponse.ok("Blinking start pattern")

    @rpc
    def on(self):
        logging.info("Turning {} ON (pin: {})".format(self.name, self.pin))
        try:
            self.device.on()
            with ClusterRpcProxy(rpc_config) as r:
                r.event.post(self.name, "on", "success", "web")
        except Exception as e:
            with ClusterRpcProxy(rpc_config) as r:
                r.event.post(self.name, "on", str(e), "web")
        return ActionResponse.ok("turned on")

    @rpc
    def off(self):
        logging.info("Turning {} OFF (pin: {})".format(self.name, self.pin))
        self.device.off()
        return ActionResponse.ok("turned off")

    @rpc
    def devstatus(self):
        logging.info("Status request for {}".format(self.name))
        _status = self.get_status()
        return ActionResponse.ok(_status)
