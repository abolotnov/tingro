#!/usr/bin/env bash
export PIGPIO_ADDR="raspberrypi"
nohup nameko run rpc --config rpc/config.yaml > nameko.log &
nohup python scheduler.py > scheduler.log &
nohup python web/manage.py runserver > django.log &