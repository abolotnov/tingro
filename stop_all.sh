#!/usr/bin/env bash
if pgrep -f nameko; then pgrep -f nameko | xargs kill; else echo "no nameko to kill"; fi
if pgrep -f runserver; then pgrep -f runserver | xargs kill; else echo "no django to kill"; fi
if pgrep -f scheduler.py; then pgrep -f scheduler.py | xargs kill; else echo "no scheduler to kill"; fi